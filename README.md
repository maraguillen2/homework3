# Operating system lab homework 3
## Homework Description
1. Creat a branch 
    * use command : git branch [your student ID]  
    * for example : git branch M11015034
2. Change branch 
    * use command : git checkout [your student ID]  
    * for example : git checkout M11015034
3. Please add your student ID at the bottom of this file, please enter the order in front
4. Creat an empty file in this folder
    * file name : studentID_date(YYYYMMDD)
    * for example : M11015034_20220512
5. Please enter your student ID in quotation marks when committing     
    * for example : git commit -m "M11015034"
6. Push the modified content to the remote repository
    * use command : git push --set-upstream origin [your student ID]
    * for example : git push --set-upstream origin M11015034

    * Please use your account
    * Username : xxx@xxx.xxx.xx  Password : xxxx
7. Create merge request

## Student ID
1. M11015034
